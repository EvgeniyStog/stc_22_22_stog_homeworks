import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String carNumber = parts[0];
        String carModel = parts[1];
        String carColor = parts[2];
        Integer carMileage = Integer.parseInt(parts[3]);
        Integer carPrice = Integer.parseInt(parts[4]);
        return new Car(carNumber, carModel, carColor, carMileage, carPrice);
    };
    private static final Function<Car, String> carToStringMapper = car ->
            car.getCarNumber() + "|" + car.getCarModel() + "|" + car.getCarColor() + "|" +
                    car.getCarMileage() + "|" + car.getCarPrice();
    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public List<Car> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<String> numberBlackCarsWithoutMileage() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getCarMileage() == 0 || car.getCarColor().equals("Black"))
                    .map(Car::getCarNumber)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public long countUniqueModels() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getCarPrice() > 700 && car.getCarPrice() < 800)
                    .map(Car::getCarModel)
                    .distinct()
                    .count();

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String colorMinCarPrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingInt(Car::getCarPrice))
                    .map(Car::getCarColor)
                    .get();

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double findAverageCamryPrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getCarModel().equals("Camry"))
                    .mapToInt(Car::getCarPrice)
                    .average()
                    .getAsDouble();

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void save(Car car) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String carToSave = carToStringMapper.apply(car);
            bufferedWriter.write(carToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}

