import java.util.List;

public interface CarsRepository {
    List<Car> findAll();

    List<String> numberBlackCarsWithoutMileage();

    long countUniqueModels();

    String colorMinCarPrice();

    double findAverageCamryPrice();

    void save(Car car);

}
