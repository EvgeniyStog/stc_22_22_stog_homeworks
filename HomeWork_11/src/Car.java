public class Car {
    private String carNumber;
    private String carModel;
    private String carColor;
    private Integer carMileage;
    private Integer carPrice;

    public Car(String carNumber, String carModel, String carColor, Integer carMileage, Integer carPrice) {
        this.carNumber = carNumber;
        this.carModel = carModel;
        this.carColor = carColor;
        this.carMileage = carMileage;
        this.carPrice = carPrice;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarColor() {
        return carColor;
    }

    public Integer getCarMileage() {
        return carMileage;
    }

    public Integer getCarPrice() {
        return carPrice;
    }

    public String getCarNumber() {
        return carNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carNumber='" + carNumber + '\'' +
                ", carModel='" + carModel + '\'' +
                ", carColor='" + carColor + '\'' +
                ", carMileage=" + carMileage +
                ", carPrice=" + carPrice +
                '}';
    }
}
