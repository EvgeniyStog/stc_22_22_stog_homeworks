public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("input.txt");
        System.out.println(carsRepository.numberBlackCarsWithoutMileage());
        System.out.println(carsRepository.countUniqueModels());
        System.out.println(carsRepository.colorMinCarPrice());
        System.out.println(carsRepository.findAverageCamryPrice());
    }
}