import java.io.IOException;

public class Main {
    public static void main(String[] args){

        ProductsRepository productRepository1 = new ProductsRepositoryFileBasedImpl("file.txt");
        System.out.println(productRepository1.findAllByTitleLike("оло"));
        Product milk = productRepository1.findById(1);
        System.out.println(milk);
        milk.setPrice(49.5);
        productRepository1.update(milk);
    }
}
