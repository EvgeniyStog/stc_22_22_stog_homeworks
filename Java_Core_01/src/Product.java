public class Product {
    private Integer id;
    private String name;
    private Double price;
    private Integer remains;

    public Product(Integer id, String name, Double price, Integer remains) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.remains = remains;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", remains=" + remains +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getRemains() {
        return remains;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setRemains(Integer remains) {
        this.remains = remains;
    }
}
