import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();

    List<Product> findAllByTitleLike(String title);

    void save(Product product);
    public void update(Product product);

    Product findById(Integer id);
}





