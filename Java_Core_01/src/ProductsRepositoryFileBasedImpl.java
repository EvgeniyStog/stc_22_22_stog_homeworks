

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;

    }

    private static final Function<String, Product> stringToProductMapper = currentUser -> {
        String[] parts = currentUser.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.valueOf(parts[2]);
        Integer remains = Integer.parseInt(parts[3]);
        return new Product(id, name, price, remains);
    };
    private final Function<Product, String> userToStringMapper = product -> product
            .getId() + "|" + product.getName() + "|" + product.getPrice() + "|" + product.getRemains();


    @Override
    public List<Product> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(stringToProductMapper).collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);

        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(stringToProductMapper).filter(it -> it.getName()
                    .toLowerCase().contains(title.toLowerCase())).toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }


    @Override
    public void save(Product product) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String productToSave = userToStringMapper.apply(product);
            bufferedWriter.write(productToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }

    public void saveAll(List<Product> products) {
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            StringBuilder stringBuilder = new StringBuilder();
            for (Product product : products) {
                stringBuilder.append(userToStringMapper.apply(product) + "\n");
            }
            bufferedWriter.write(stringBuilder.toString());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName)))
              {
            List<Product> productList = reader.lines().map(stringToProductMapper).toList();
            Product oldProduct = productList.stream().filter(it -> it.getId().equals(product.getId())).findFirst().get();
            Product newProduct = new Product(oldProduct.getId(), product.getName(), product.getPrice(), product.getRemains());
            List<Product> products = productList.stream().map(it -> {
                if (it.getId() == newProduct.getId()) {
                    return newProduct;
                }
                return it;
            }).toList();
            saveAll(products);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }


    @Override
    public Product findById(Integer id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(stringToProductMapper)
                    .filter(it -> it.getId().equals(id)).findFirst().orElse(null);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }


}
