public class Main {
    public static void main(String[] args) {
        Task[] tasks = new Task[]{new EvenNumbersPrintTask(3, 8), new OddNumbersPrintTask(11, 23),
                new OddNumbersPrintTask(32, 48)};
        completeAllTasks(tasks);
    }

    private static void completeAllTasks(Task[] tasks){
        for (Task task : tasks) {
            task.complete();
        }
    }
}