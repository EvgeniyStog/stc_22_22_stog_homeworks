import java.util.Scanner;

public abstract class AbstractNumbersPrintTask implements Task{
 int from, to;
Scanner sc = new Scanner(System.in);


    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }
    @Override
    public abstract void complete();

}
