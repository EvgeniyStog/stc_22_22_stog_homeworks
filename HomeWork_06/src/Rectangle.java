public class Rectangle extends Square {
    protected int b;

    public Rectangle(int x, int y, int a, int b) {
        super(x, y, a);
        this.b = b;
    }


    @Override
    protected double sq() {
        return s = a * b;
    }

    @Override
    protected double perimetr() {
        return p = a * 2 + b * 2;
    }
    @Override
    protected void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
