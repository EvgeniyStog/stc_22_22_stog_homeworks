public class Circle extends Figure {
    protected int radius;
    double pi = 3.1415926;

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }


    @Override
    protected void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    protected double sq() {
        return s = radius * radius * pi;
    }

    @Override
    protected double perimetr() {
        return p = 2 * pi * radius;
    }
}

