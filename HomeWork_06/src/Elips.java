public class Elips extends Circle {
    int smallRadius;

    public Elips(int x, int y, int radius, int smallRadius) {
        super(x, y, radius);
        this.smallRadius = smallRadius;
    }


    @Override
    protected double sq() {
        return s = radius * smallRadius * pi;
    }
    @Override
    protected void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    protected double perimetr() {
        return p = 4 * (Math.PI * radius * smallRadius + (radius - smallRadius) * (radius - smallRadius)) / (radius + smallRadius);
    }
}

