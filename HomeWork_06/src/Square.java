public class Square extends Figure {


    public Square(int x, int y, int a) {
        super(x, y);
        this.a = a;
    }

    @Override
    protected double perimetr() {
        return p = a * 4;
    }

    @Override
    protected void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    protected double sq() {
        return s = a * a;

    }
}