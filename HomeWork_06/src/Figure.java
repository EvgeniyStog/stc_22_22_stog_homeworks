import java.util.Scanner;

public abstract class Figure {
    //protected int centre;
    protected int x;
    protected int y;
    protected int a;
    protected double s;
    protected double p;
    public Figure(int x, int y){};

    protected abstract void move(int x, int y);

    protected abstract double sq();

    protected abstract double perimetr();
}
