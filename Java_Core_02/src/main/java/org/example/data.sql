

insert into "user" (first_name, last_name, phone_number, experience, age, drivers_license, categoryy, rating)
values ('Andreey', 'Tsiqsher', 86841269145, 3, 24,true, 'b', 5);
insert into "user" (first_name, last_name, phone_number, experience, age, drivers_license, categoryy, rating)
values ('Andrey', 'Tsisher', 86841239145, 9, 27,true, 'b', 5);
insert into "user" (first_name, last_name, phone_number, experience, age, drivers_license, categoryy, rating)
values ('Evgeniy', 'Furman', 89511323145, 15, 35,true, 'b', 5);
insert into "user" (first_name, last_name, phone_number, experience, age, drivers_license, categoryy, rating)
values ('Andrey', 'Abramov', 89211239145, 0, 32,false, ' ', 0);
insert into "user" (first_name, last_name, phone_number, experience, age, drivers_license, categoryy, rating)
values ('Max', 'Petrov', '89226455689', 3, 23, true, 'b', 4);

insert into car (model, colour, number)
values ('audi', 'black', '777');
insert into car (model, colour, number)
values ('audi', 'white', '666');
insert into car (model, colour, number)
values ('bmw', 'gray', '111');
insert into car (model, colour, number)
values ('bmw', 'black', '000');
insert into car (model, colour, number)
values ('lada', 'rusty', '197');


insert into trip (id_user, id_car, data, duration)
values (2,3,'21.01,2022', '35 min');
insert into trip (id_user, id_car, data, duration)
values (4,3,'21.03,2022', '21 min');
insert into trip (id_user, id_car, data, duration)
values (1,2,'21.05,2022', '33 min');
insert into trip (id_user, id_car, data, duration)
values (2,5,'12.01,2022', '48 min');
insert into trip (id_user, id_car, data, duration)
values (1,1, '18.01,2022', '22 min');


