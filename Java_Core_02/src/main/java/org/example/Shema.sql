drop table if exists "user";
drop table trip;
drop table if exists course;


create table "user"
(
    id              serial primary key,
    first_name      varchar(20),
    last_name       varchar(20),
    "phone_number"  varchar,
    experience      integer check (experience >= 0 and experience <= 60),
    age             integer check (age >= 0 and age <= 120),
    drivers_license bool,
    categoryy       varchar,
    rating          integer check ( rating >= 0 and rating <= 5)
);


create table car
(
    model     char(20) not null,
    colour    char(20),
    number    varchar,
    id_driver serial primary key
);

create table trip
(
    id_user  bigint,
    id_car   integer,
    data     varchar,
    duration varchar,
    foreign key (id_user) references "user" (id),
    foreign key (id_car) references "car" (id_driver)
);
