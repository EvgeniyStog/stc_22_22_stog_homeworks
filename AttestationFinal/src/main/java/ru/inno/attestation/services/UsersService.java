package ru.inno.attestation.services;

import ru.inno.attestation.dto.UserForm;
import ru.inno.attestation.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}

