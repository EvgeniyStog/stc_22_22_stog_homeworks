package ru.inno.attestation.services;

import ru.inno.attestation.models.User;
import ru.inno.attestation.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}
