public class Main {
    public static void main(String[] args) {

        ArrayTask sumArrayFromTo = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            System.out.println("\r");
            return sum;
        };

        ArrayTask maxNumberInRange = (array, from, to) -> {
            int max = array[from];
            for (int i = from; i < to; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            System.out.println("\rМаксимальное число в диапазоне: " + max);
            int num = max;
            int sum = 0;
            while (num > 0) {
                sum += num % 10;
                num /= 10;
            }
            return sum;
        };
        try {

        int[] array2 = {3, 8, 12, 34, 99, 45, 35, 1, 69};
        int[] array = {4, 3, 8, 28, 7, 11, 9, 1, 15, 14};
        ArraysTasksResolver.resolveTask(array, sumArrayFromTo, 3, 6);
        ArraysTasksResolver.resolveTask(array, maxNumberInRange, 3, 6);
        ArraysTasksResolver.resolveTask(array2,sumArrayFromTo, 5, 7);}
        catch (java.lang.ArrayIndexOutOfBoundsException e) {
            System.out.println( "\n\u001B[31m ошибка java.lang.ArrayIndexOutOfBoundsException Введен несуществующий индекс массива");

        }
    }
}
