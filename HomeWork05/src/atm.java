public class Atm {
    private int balance;
    private final int maxOutput;
    private final int maxBalance;

    private int transaction;

    public Atm(int balance, int maxOutput, int maxBalance, int transaction) {
        this.balance = balance;
        this.maxOutput = maxOutput;
        this.maxBalance = maxBalance;
        this.transaction = transaction;
    }

    public int receive(int getMe) {
        transaction++;
        if (getMe > balance || getMe > maxOutput) {
           return 0;
        }
        balance = balance - getMe;
        return getMe;
    }

    public int insert(int getMe) {
        transaction++;
        if (getMe + balance > maxBalance) {
            int balance1 = balance;
            balance = maxBalance;
            return getMe + balance1 - maxBalance;
        }
        return 0;
    }
}